# Copyright 2017 Aleksandr Polyakov. All rights reserved.
# Use of this source code is governed by a MIT-style
# license that can be found in the LICENSE file.
eq = $(and $(findstring x$(1),x$(2)), $(findstring x$(2),x$(1)))

APP=gateway
PROJECT=gitlab.com/dantser/backend/gateway
REGISTRY?=registry.gitlab.com/founder-sl
CA_DIR?=certs

# Use the 0.0.0 tag for testing, it shouldn't clobber any release builds
RELEASE?=0.0.14
GOOS?=linux
GOARCH?=amd64

API_GATEWAY_LOCAL_HOST?=0.0.0.0
API_GATEWAY_LOCAL_PORT?=8080
API_GATEWAY_LOG_LEVEL?=0
API_GATEWAY_MONGO_HOST?=mongodb://mongodb.db/admin
API_GATEWAY_TOKEN_SECRET?=secret

# Namespace: dev, prod, release, cte, username ...
NAMESPACE?=prod

# Infrastructure: dev, stable, test ...
INFRASTRUCTURE?=stable
VALUES?=values-${INFRASTRUCTURE}

#CONTAINER_IMAGE?=${REGISTRY}/${APP}
CONTAINER_IMAGE?=${REGISTRY}/dan-gateway
CONTAINER_TAG?=$(if $(or $(call eq,${NAMESPACE},prod),$(call eq,${NAMESPACE},staging)),${RELEASE},review-${NAMESPACE})
CONTAINER_NAME?=${APP}-${NAMESPACE}

REPO_INFO=$(shell git config --get remote.origin.url)

ifndef CI_COMMIT_SHA
CI_COMMIT_SHA := git-$(shell git rev-parse HEAD)
endif

BUILDTAGS=

.PHONY: all
all: build

.PHONY: vendor
vendor: clean bootstrap
	dep ensure

.PHONY: build
build: vendor
	@echo "+ $@"
	@CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} go build -a -installsuffix cgo \
		-ldflags "-s -w -X ${PROJECT}/pkg/version.RELEASE=${RELEASE} -X ${PROJECT}/pkg/version.COMMIT=${CI_COMMIT_SHA} -X ${PROJECT}/pkg/version.REPO=${REPO_INFO} -X ${PROJECT}/pkg/version.NAMESPACE=${NAMESPACE}" \
		-o bin/${GOOS}-${GOARCH}/${APP} ${PROJECT}/cmd

bin/${GOOS}-${GOARCH}/${APP}: build

.PHONY: build-image
build-image: certs
	docker build --pull -t $(CONTAINER_IMAGE):$(CONTAINER_TAG) .

.PHONY: certs
certs:
ifeq ("$(wildcard $(CA_DIR)/ca-certificates.crt)","")
	@echo "+ $@"
	@docker run --name ${CONTAINER_NAME}-certs -d alpine:latest sh -c "apk --update upgrade && apk add ca-certificates && update-ca-certificates"
	@docker wait ${CONTAINER_NAME}-certs
	@mkdir -p ${CA_DIR}
	@docker cp ${CONTAINER_NAME}-certs:/etc/ssl/certs/ca-certificates.crt ${CA_DIR}
	@docker rm -f ${CONTAINER_NAME}-certs
endif

.PHONY: push
push: build-image
	@echo "+ $@"
	@docker push $(CONTAINER_IMAGE):$(CONTAINER_TAG)

.PHONY: run
run: build
	@echo "+ $@"
	@docker run --name ${CONTAINER_NAME} -p ${API_GATEWAY_LOCAL_PORT}:${API_GATEWAY_LOCAL_PORT} \
		-e "API_GATEWAY_LOCAL_HOST=${API_GATEWAY_LOCAL_HOST}" \
		-e "API_GATEWAY_LOCAL_PORT=${API_GATEWAY_LOCAL_PORT}" \
		-e "API_GATEWAY_LOG_LEVEL=${API_GATEWAY_LOG_LEVEL}" \
		-e "API_GATEWAY_MONGO_HOST=${API_GATEWAY_MONGO_HOST}" \
		--link mongo:mongo \
		--link account-stable:accounts \
		-d $(CONTAINER_IMAGE):$(CONTAINER_TAG)
	@sleep 1
	@docker logs ${CONTAINER_NAME}

HAS_RUNNED := $(shell docker ps | grep ${CONTAINER_NAME})
HAS_EXITED := $(shell docker ps -a | grep ${CONTAINER_NAME})

.PHONY: logs
logs:
	@echo "+ $@"
	@docker logs ${CONTAINER_NAME}

.PHONY: stop
stop:
ifdef HAS_RUNNED
	@echo "+ $@"
	@docker stop ${CONTAINER_NAME}
endif

.PHONY: start
start: stop
	@echo "+ $@"
	@docker start ${CONTAINER_NAME}

.PHONY: rm
rm:
ifdef HAS_EXITED
	@echo "+ $@"
	@docker rm ${CONTAINER_NAME}
endif

.PHONY: deploy
deploy: push
	helm upgrade ${CONTAINER_NAME} -f charts/${VALUES}.yaml --set image.tag=${CONTAINER_TAG} charts --namespace ${NAMESPACE} --version=${RELEASE} -i --wait

GO_LIST_FILES=$(shell go list ${PROJECT}/... | grep -v vendor)

.PHONY: fmt
fmt:
	@echo "+ $@"
	@go list -f '{{if len .TestGoFiles}}"gofmt -s -l {{.Dir}}"{{end}}' ${GO_LIST_FILES} | xargs -L 1 sh -c

.PHONY: lint
lint: bootstrap
	@echo "+ $@"
	@go list -f '{{if len .TestGoFiles}}"golint -min_confidence=0.85 {{.Dir}}/..."{{end}}' ${GO_LIST_FILES} | xargs -L 1 sh -c

.PHONY: vet
vet:
	@echo "+ $@"
	@go vet ${GO_LIST_FILES}

.PHONY: test
test: vendor fmt lint vet
	@echo "+ $@"
	@go test -v -race -cover -tags "$(BUILDTAGS) cgo" ${GO_LIST_FILES}

.PHONY: cover
cover:
	@echo "+ $@"
	@> coverage.txt
	@go list -f '{{if len .TestGoFiles}}"go test -coverprofile={{.Dir}}/.coverprofile {{.ImportPath}} && cat {{.Dir}}/.coverprofile  >> coverage.txt"{{end}}' ${GO_LIST_FILES} | xargs -L 1 sh -c

.PHONY: clean
clean: stop rm
	@rm -f bin/${GOOS}-${GOARCH}/${APP}

HAS_DEP := $(shell command -v dep;)
HAS_LINT := $(shell command -v golint;)

.PHONY: bootstrap
bootstrap:
ifndef HAS_DEP
	go get -u github.com/golang/dep/cmd/dep
endif
ifndef HAS_LINT
	go get -u github.com/golang/lint/golint
endif
