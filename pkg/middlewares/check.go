package middlewares

import (
	"bytes"
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"encoding/json"

	"github.com/gin-gonic/gin"
	"gitlab.com/dantser/backend/gateway/pkg/config"
)

// WhiteRoutes not check token
var WhiteRoutes = map[string]bool{
	"/SignUp":     true,
	"/SignIn":     true,
	"/ResendCode": true,
	"/Confirm":    true,
}

// CheckAuth - check auth middleware
func CheckAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		cfg := config.GetMainConfig()

		module := c.Param("module")
		path := c.Param("path")

		if module == "account" && WhiteRoutes[path] {
			c.Next()
			return
		}

		SID, err := c.Cookie("SID")
		if err == http.ErrNoCookie {
			log.Print("Not found SID cookie in request")
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			c.Abort()
			return
		}

		AID, accessErr := c.Cookie("AID")
		RID, refreshErr := c.Cookie("RID")
		if accessErr == http.ErrNoCookie && refreshErr == http.ErrNoCookie {
			log.Print("Not found AID or RID cookies in request")
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			c.Abort()
			return
		}

		// check user info
		// POST /CheckSession
		// {"SID":"", "AID":"", "RID":""}
		sessions := []byte(`{"SID":"` + SID + `", "AID":"` + AID + `", "RID":"` + RID + `"}`)
		resp, err := http.Post("http://"+cfg.AccountHost+"/CheckSessionAndGetUser", "application/json", bytes.NewBuffer(sessions))
		if err != nil {
			log.Printf("Error in request user sessions: %s", err.Error())
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			c.Abort()
			return
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			log.Printf("Bad response: %v", resp.StatusCode)
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			c.Abort()
			return
		}

		respData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("Error read body: %s", err.Error())
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			c.Abort()
			return
		}
		var data struct {
			UserID string `json:"user_id"`
			AID    string `json:"AID"`
			RID    string `json:"RID"`
		}
		if err = json.Unmarshal(respData, &data); err != nil {
			log.Printf("Error unmarshal body: %s", err.Error())
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			c.Abort()
			return
		}
		if len(data.AID) > 0 && data.AID != AID {
			c.SetCookie("AID", data.AID, cfg.CookieAidMaxAge, cfg.CookiePath, cfg.CookieDomain, cfg.UseSecureCookie, cfg.UseHttpOnlyCookie)
		}
		if len(data.RID) > 0 && data.RID != RID {
			c.SetCookie("RID", data.RID, cfg.CookieRidMaxAge, cfg.CookiePath, cfg.CookieDomain, cfg.UseSecureCookie, cfg.UseHttpOnlyCookie)
		}

		if len(data.UserID) <= 0 {
			log.Println("user id is fail")
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			c.Abort()
			return
		}

		c.Request.Header.Set("DMS-ACCOUNT-ID", data.UserID)

		_, err = CheckPost(c, data.UserID)
		if err != nil {
			log.Println("User can't access to post")
			c.String(http.StatusForbidden, http.StatusText(http.StatusForbidden))
			c.Abort()
			return
		}

		c.Next()
		return

	}
}

// CheckPost check access to post
func CheckPost(c *gin.Context, accountID string) (bool, error) {
    
	postID := c.GetHeader("Dms-Post-Id")

	if postID == "" {
		//Post ID don't set
		return false, nil
	}
    cfg := config.GetMainConfig() 

	responseData, err := json.Marshal(map[string]interface{}{
		"account_id": accountID,
		"post_id":    postID,
	})
	if err != nil {
		return true, err
	}

	resp, err := http.Post("http://"+cfg.OrgstructureHost+"/check-access", "application/json", bytes.NewBuffer(responseData))
	if err != nil {
		return true, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return true, errors.New("You can`n access")
	}

	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return true, err
	}

	var data struct {
		CompanyID    string `json:"company_id"`
		DepartmentID string `json:"container_id"`
		PostID       string `json:"post_id"`
		IsMy         bool   `json:"is_my"`
	}
	if err = json.Unmarshal(respData, &data); err != nil {
		return true, err
	}

	c.Request.Header.Set("DMS-COMPANY-ID", data.CompanyID)
	c.Request.Header.Set("DMS-DEPARTMENT-ID", data.DepartmentID)
	c.Request.Header.Set("DMS-POST-ID", data.PostID)

	return true, nil
}
